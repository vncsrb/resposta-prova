package utfpr.ct.dainf.pratica;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 *
 * @author
 */
public class Ponto {
    private double x, y, z;

    /**
     * Retorna o nome não qualificado da classe.
     * @return O nome não qualificado da classe.
     */
    public String getNome() {
        return getClass().getSimpleName();
    }
    
    public Ponto(){
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }
    public Ponto (double x, double y, double z){
        this.x=x;
        this.y=y;
        this.z=z;
    }
   public double dist(Ponto p2){
        double d, difx, dify, difz;
        difx = p2.x - this.x;
        dify = p2.y - this.y;
        difz = p2.z - this.z;
        d = sqrt(pow(difx, 2) + pow(dify, 2) + pow(difz,2));
        return d;
    }
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }
    

}
