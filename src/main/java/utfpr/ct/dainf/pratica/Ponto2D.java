/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.pratica;

/**
 *
 * @author Vinicius
 */
abstract class Ponto2D extends Ponto {
    
    public Ponto2D(){
        super();
    }
    
    public Ponto2D(double x, double y, double z)
    {
       super(x,y,z);
    }
    public class PontoXY extends Ponto2D {
        public PontoXY(){
        super();
    }
    
    public PontoXY(double x, double y){
       super(x, y, 0);
    } 
}
    public class PontoXZ extends Ponto2D {
        public PontoXZ(){
        super();
    }
    
    public PontoXZ  (double x, double z){
       super(x,0,z);
    }
}
    public class PontoYZ extends Ponto2D {
        public PontoYZ(){
        super();
    }
 
    public PontoYZ  (double y, double z){
       super(0,y,z);
    }
}
}

